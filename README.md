Handy, time-saving snippets of code for web design.

* **[Favicons](https://github.com/rhysdoesdesign/Meta-Snippets/blob/master/favicons.html)** - Favicon template code, made to be compatible with as many browsers, devices and resolutions as possible.
* **[OGP](https://github.com/rhysdoesdesign/Meta-Snippets/blob/master/ogp.html)** - A sample of Open Graph Protocol meta tags.
